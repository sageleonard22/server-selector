package me.goswimmy.hubaddon.events;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.goswimmy.hubaddon.managers.ItemManager;

public class JoinEvent implements Listener {

	@SuppressWarnings("deprecation")
	@EventHandler
	public void Join(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		
//		if(p.hasPlayedBefore()) {
//			p.sendTitle(ChatColor.translateAlternateColorCodes('&', "&f&l&oBRUTEWARS"), ChatColor.translateAlternateColorCodes('&', "&7&oWelcome back, "+p.getName()+"!"), 10, 30, 10);
//		} else {
//			p.sendTitle(ChatColor.translateAlternateColorCodes('&', "&f&l&oBRUTEWARS"), ChatColor.translateAlternateColorCodes('&', "&7&oWelcome, "+p.getName()+"!"), 10, 30, 10);
//		}
		
		ItemManager.setInv(p);
		p.setMaxHealth(10D);
		Bukkit.getServer().dispatchCommand(p, "spawn");
		p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 1, 5, true, false));
		p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 1, 5, true, false));
//		p.playSound(p.getLocation(), Sound.AMBIENT_CAVE, 1, 1);
	}
}
