package me.goswimmy.hubaddon.events;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import me.goswimmy.hubaddon.managers.ItemManager;
import me.goswimmy.hubaddon.managers.ServerSelectorManager;
import me.goswimmy.hubaddon.managers.TeamManager;

public class UseItemEvent implements Listener {

	@EventHandler
	public void useItem(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		ItemStack i = e.getItem();
		if (i != null && i.getType() != Material.AIR) {
			if (i.getType() == Material.REDSTONE_TORCH) {
				TeamManager.toggleHide(p);
				p.getInventory().setItem(6, ItemManager.torch());
			}
			if (i.getType() == Material.TORCH) {
				TeamManager.toggleHide(p);
				p.getInventory().setItem(6, ItemManager.redstone_torch());
			}
			if (i.getType() == Material.COMPASS) {
				ServerSelectorManager.openGui(p);
			}
		}
	}
}
