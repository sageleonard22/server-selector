package me.goswimmy.hubaddon.events;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import me.goswimmy.hubaddon.Main;
import net.md_5.bungee.api.ChatColor;

public class ServerSelectorEvent implements Listener {

	@EventHandler
	public void onClick(InventoryClickEvent e) {
		if (e.getWhoClicked() instanceof Player) {
			Player p = (Player) e.getWhoClicked();
			if (e.getView().getTitle().equals(ChatColor.translateAlternateColorCodes('&', "&8Choose a server..."))) {
				e.setCancelled(true);
				if (e.getCurrentItem() != null && e.getCurrentItem().getType() != Material.AIR) {
					ItemStack i = e.getCurrentItem();
					if (i.getType() == Material.OAK_DOOR) {
						p.closeInventory();
					}
					if (i.getType() == Material.ANVIL) {
						ByteArrayDataOutput out = ByteStreams.newDataOutput();
						out.writeUTF("Connect");
						out.writeUTF("worldpvp");
						p.sendPluginMessage(Main.getPlugin(Main.class), "BungeeCord", out.toByteArray());
					}
					if (i.getType() == Material.ENCHANTED_GOLDEN_APPLE) {
						Bukkit.getServer().dispatchCommand(p, "warp duels");
					}
					if (i.getType() == Material.WOODEN_SWORD) {
						ByteArrayDataOutput out = ByteStreams.newDataOutput();
						out.writeUTF("Connect");
						out.writeUTF("kitpvp");
						p.sendPluginMessage(Main.getPlugin(Main.class), "BungeeCord", out.toByteArray());
					}
				}
				return;
			}
			e.setCancelled(true);
		}
	}
}
