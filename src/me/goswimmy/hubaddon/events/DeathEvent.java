package me.goswimmy.hubaddon.events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import me.goswimmy.hubaddon.managers.ItemManager;

public class DeathEvent implements Listener {

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onDeath(PlayerRespawnEvent e) {
		Player p = e.getPlayer();
		p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1, true, false), false);
		ItemManager.setInv(p);
	}
}
