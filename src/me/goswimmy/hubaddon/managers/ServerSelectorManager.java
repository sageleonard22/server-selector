package me.goswimmy.hubaddon.managers;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.md_5.bungee.api.ChatColor;

public class ServerSelectorManager {

	public static void openGui(Player p) {
		Inventory inv = Bukkit.createInventory(null, InventoryType.HOPPER, ChatColor.translateAlternateColorCodes('&', "&8Choose a server..."));
		
		ItemStack door = new ItemStack(Material.OAK_DOOR);
		ItemMeta doorm = door.getItemMeta();
		doorm.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&fExit Menu"));
		door.setItemMeta(doorm);
		
		ItemStack egapple = new ItemStack(Material.ENCHANTED_GOLDEN_APPLE);
		ItemMeta egapplem = egapple.getItemMeta();
		egapplem.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&3Duels"));
		egapple.setItemMeta(egapplem);
		
		ItemStack anvil = new ItemStack(Material.ANVIL);
		ItemMeta anvilm = anvil.getItemMeta();
		anvilm.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&bWorldPvP"));
		anvil.setItemMeta(anvilm);
		
		ItemStack wsword = new ItemStack(Material.WOODEN_SWORD);
		ItemMeta wswordm = wsword.getItemMeta();
		wswordm.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&1KitPvP"));
		wsword.setItemMeta(wswordm);
		
		inv.setItem(0, door);
		inv.setItem(2, egapple);
		inv.setItem(3, anvil);
		inv.setItem(4, wsword);
		
		p.openInventory(inv);
	}
}
