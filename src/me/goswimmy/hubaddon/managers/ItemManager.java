package me.goswimmy.hubaddon.managers;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.md_5.bungee.api.ChatColor;

public class ItemManager {
	
	public static void setInv(Player p) {
		p.getInventory().clear();
		p.getInventory().setItem(6, torch());
		p.getInventory().setItem(2, compass());
	}
	
	public static ItemStack compass() {
		ItemStack i = new ItemStack(Material.COMPASS);
		ItemMeta m = i.getItemMeta();
		m.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&a&kw&2&oServers"));
		i.setItemMeta(m);
		return i;
	}
	
	public static ItemStack redstone_torch() {
		ItemStack i = new ItemStack(Material.REDSTONE_TORCH);
		ItemMeta m = i.getItemMeta();
		m.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&cPlayers Hidden"));
		i.setItemMeta(m);
		return i;
	}
	
	public static ItemStack torch() {
		ItemStack i = new ItemStack(Material.TORCH);
		ItemMeta m = i.getItemMeta();
		m.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&aPlayers Shown"));
		i.setItemMeta(m);
		return i;
	}
}
