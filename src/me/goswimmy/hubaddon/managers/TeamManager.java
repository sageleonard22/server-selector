package me.goswimmy.hubaddon.managers;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;

import net.md_5.bungee.api.ChatColor;

public class TeamManager {

	public static ScoreboardManager manager;
	public static Scoreboard board;
	public static Team ghostteam;
	public static Team hideteam;

	public static void initialize() {
		manager = Bukkit.getScoreboardManager();
		board = manager.getNewScoreboard();
		ghostteam = board.registerNewTeam("ghostteam");
		ghostteam.setCanSeeFriendlyInvisibles(true);
		ghostteam.setDisplayName("Ghost");

		hideteam = board.registerNewTeam("hideteam");
		hideteam.setCanSeeFriendlyInvisibles(false);
		hideteam.setDisplayName("Hide");
	}

	@SuppressWarnings("deprecation")
	public static void addPlayerToTeam(Player p) {
		p.setScoreboard(board);
		ghostteam.addPlayer(p);
		p.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1, true, false), false);
	}

	@SuppressWarnings("deprecation")
	public static void toggleHide(Player p) {
		if (ghostteam.hasPlayer(p)) {
			ghostteam.removePlayer(p);
			hideteam.addPlayer(p);
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7Players &8&l| &c&k|||||||"));
		} else if (hideteam.hasPlayer(p)) {
			hideteam.removePlayer(p);
			ghostteam.addPlayer(p);
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7Players &8&l| &aShown"));
		} else {
			ghostteam.addPlayer(p);
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&7Players &8&l| &aShown"));
		}
	}
}
