package me.goswimmy.hubaddon.managers;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

public class TaskManager {

	public static void startTask(Plugin pl) {
		new BukkitRunnable() {
			@SuppressWarnings("deprecation")
			@Override
			public void run() {
				for(Player all : Bukkit.getOnlinePlayers()) {
					if(!all.hasPotionEffect(PotionEffectType.INVISIBILITY)) {
						all.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, 1, true, false), false);
					}
				}
			}
		}.runTaskTimer(pl, 0, 20*1);
	}
	
}
