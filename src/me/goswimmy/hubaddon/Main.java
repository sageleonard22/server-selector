package me.goswimmy.hubaddon;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import me.goswimmy.hubaddon.events.DeathEvent;
import me.goswimmy.hubaddon.events.JoinEvent;
import me.goswimmy.hubaddon.events.ServerSelectorEvent;
import me.goswimmy.hubaddon.events.UseItemEvent;
import me.goswimmy.hubaddon.managers.ItemManager;
import me.goswimmy.hubaddon.managers.TeamManager;

public class Main extends JavaPlugin {

	@SuppressWarnings("deprecation")
	@Override
	public void onEnable() {
		this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
		
		TeamManager.initialize();
		
		// Initailize All Players
		for(Player all : Bukkit.getOnlinePlayers()) {
			TeamManager.addPlayerToTeam(all);
			ItemManager.setInv(all);
			all.setMaxHealth(10D);
		}
		
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new UseItemEvent(), this);
		pm.registerEvents(new DeathEvent(), this);
		pm.registerEvents(new ServerSelectorEvent(), this);
		pm.registerEvents(new JoinEvent(), this);
	}
}
